<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>New/Edit Student</title>
</head>
<body>
<div id="Progress_Status"> 
	<h1>New/Edit Student</h1>
    <div id="myprogressBar"></div> 
</div>
	<div align="center">
		
		<form:form action="savestudent" method="post" modelAttribute="student">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>
			<tr>
				<td>Student Name:</td>
				<td><form:input path="studname"/></td>
			</tr>
			<tr style="margin-top:40px;">
				<td>Email:</td>
				<td><form:input path="email"/></td>
			</tr>

			<tr style="margin-top:40px;">
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="Save"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>