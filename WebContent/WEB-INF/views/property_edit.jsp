<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>  
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>New/Edit Property</title>
</head>
<body>
<div id="Progress_Status"> 
	<h1>New/Edit Property</h1>
    <div id="myprogressBar"></div> 
</div>
	<div align="center">
		
		<form:form action="saveproperty" method="post" modelAttribute="property">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>Student:</td>

				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>
						<!--<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>	-->
					</form:select>
				</td>
			</tr>
			<tr>
				<td>Criteria:</td>
				<td>
					<form:select path="criteriaid">						
						<form:option value="${criteria.id}" label="${criteria.critname}"/>
						<form:options items="${listCriteria}" 
						itemValue="id" itemLabel="critname"/>	
					</form:select>
				</td>				
				
			</tr>
<!--  https://www.w3schools.com/howto/howto_css_responsive_form.asp -->
			<tr>
				<div>
					<td>Description:</td>
				</div>
			</tr>
			<tr>
				<td colspan="2" align="center"><form:textarea path="description" style="height:150px"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="SaveProperty"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>