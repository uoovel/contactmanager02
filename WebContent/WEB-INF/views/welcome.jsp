<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>   
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Insert title here</title>
</head>
<body>
<div align="center">
	<a onclick="update()" href="index">Log out</a>
</div>
<div id="Progress_Status">   
  	<h1 align ="center">Hello ${student.studname}, </h1>
	<h2 align="center">You have logged in</h2>
	<div id="myprogressBar"></div> 
</div>

<div align="center">
		<form:form action="students" method="get" modelAttribute="student">
		<table cellpadding="$">			 
			<form:hidden path="userid"/>			 
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="My Details"/></td>			
			</tr>			 
		</table>		
		</form:form>	    
	   <!-- 
	    <h3>
    		<a href="students">Students</a>
    	</h3>
	    -->
   		<form:form action="studentpacks" method="get" modelAttribute="student" style="margin-top:40px;">
   			<form:hidden path="userid"/>   		
	    	<input onclick="update()" type="submit" value="My Courses"/>	
   		</form:form>
	    <!--
	    <a href="studentpacks">My Courses</a>
	      -->
	      
</div>	      
</body>
</html>