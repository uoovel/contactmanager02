<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>      
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Students Home</title>
</head>
<body>

	<div align="center">
			
		<form:form action="gohome2" method="get" modelAttribute="student" id="form1">
			<form:hidden path="userid"/>	
		</form:form>
		<form:form action="index" method="get" id="form2">				
		</form:form>
		<table>
			<tr>
			<td>
			<input onclick="update(1)" class="button" type="submit" form="form1" value="Home" style="width:10em"/>
	        </td>
	        <td>
	        <input onclick="update()" class="button" type="submit" form="form2" value="Log out" style="width:10em"/>
			</td>
		</table>
		</div>
<div id="Progress_Status"> 
	<h1 align="center">My personal details</h1>
    <div id="myprogressBar"></div> 
  
</div>
		<div align="center">
		<!-- <h3><a href="newstudent">New Student</a> </h3> -->
		<table border="1" cellpadding="5" style="margin-top:40px;">
			<tr>
				<th>No</th>
				<th>Student Name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listStudent}" var="student" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${student.studname}</td>
				<td>${student.email}</td>
				<td>
					<a onclick="update()" href="editstudent?id=${student.id }">Edit</a>
					&nbsp;&nbsp;
					<!--  <a href="deletestudent?id=${student.id }">Delete</a> -->
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
	<!-- properties -->
		<div align="center">		
		
		<!-- <h3 style="color:#955">!Links below are under development!</h3> -->
		
		<form:form action="newproperty" method="get" modelAttribute="student">
			<form:hidden path="userid"/>
			<table>
			<tr>
			<td>
			<h2 align="center">Properties List</h2>	 		
	   		</td>
	   		<td>
	   		<input onclick="update(2)" type="submit" value="New Property" style="width:10em"/>	
		    </td>
		    </tr>
		    </table>
		</form:form>
	</div>
	
	 
		  <div id="myprogressBar2" style="margin-top:0px;"></div>
		   
		
		
	<div align="center" style="margin-top:0px;">
		<table border="1" cellpadding="5" class="gfh" style="margin-top:0px;">
			<tr>
				<th>No</th>
				<!-- <th>Student</th> -->
				<th>Criteria</th>
				<th>Description</th>

				<th>Action</th>
			</tr>
			<c:forEach items="${listPropertyView}" var="propertyview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<!--  
				<td style="width:50px">${propertyview.studname}</td>
				<td style="width:100px">${propertyview.critname}</td>
				<td style="width:300px">${propertyview.description}</td>
				-->
				<!-- <td>${propertyview.studname}</td> -->
				<td style="width:7em;">${propertyview.critname}</td>
				<td style="width:15em;">${propertyview.description}</td>
				

				<td>
					<a onclick="update(2)" href="editproperty?id=${propertyview.id }">Edit</a>
					&nbsp;&nbsp;
					<div>
					<a onclick="update(2)" href="deleteproperty?id=${propertyview.id }">Delete</a>
					</div>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
	
</body>
</html>