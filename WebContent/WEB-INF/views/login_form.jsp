<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
	<script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Login form</title>
<style>
.gfg { 
        border-collapse:separate; 
        border-spacing:0 15px; 
}
</style>
</head>
<body>
<div id="Progress_Status"> 
	<h1>Login to account</h1>
    <div id="myprogressBar"></div> 
</div>
	<div align="center">
		
		<form:form action="loginUser" method="post" modelAttribute="user">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>
			<!--  
			<tr style="padding-top: 40px; padding-bottom:20px;>
				<td>Student Name:</td>
				<td><form:input path="uname"/></td>			
			</tr>	
			-->		
			<tr>
				<td>Email:</td>
				<td><form:input path="email" style="border:1px solid black;"/></td>			
			</tr>
			
			<tr>
				<td>Password:</td>
				<td><form:input path="password" type="password" style="border:1px solid black;"/></td>			
			</tr>
			
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="Login"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>