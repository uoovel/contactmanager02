<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Properties Home</title>
</head>
<body>
	<div align="center">
		<form:form action="teachergohome3" method="get" modelAttribute="teacher">
			<form:hidden path="userid"/>	 		
	   		<input onclick="update()" type="submit" value="Home"/>	
		</form:form>
	</div>
	<div id="Progress_Status"> 
		<h1 align="center">Properties List</h1>
	  	<div id="myprogressBar"></div> 
	</div>
	<div align="center">

		
		<h2>Student: ${student.studname} &nbsp;
		<a onclick="update()" href="ratings?studentid=${student.id}&teacherid=${teacher.id}" align="right">Ratings</a>
		</h2>
		<!-- <h3><a href="newproperty">New Property</a> </h3> -->
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<!-- <th>Student</th> -->
				<th>Criteria</th>
				<th>Description</th>
				<!-- <th>Action</th> -->
				<!--  <th>Action</th>-->
			</tr>
			<c:forEach items="${listPropertyView}" var="propertyview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<!-- <td>${propertyview.studname}</td> -->
				<td>${propertyview.critname}</td>
				<td style="width:15em;">${propertyview.description}</td>
				<!--  <td>
					<a href="ratings?studentid=${propertyview.studentid}&teacherid=${propertyview.teacherid}">Ratings</a>
				  
				</td>-->
<!-- 
				<td>
					<a href="editproperty?id=${propertyview.id }">Edit</a>
					&nbsp;&nbsp;
					<a href="deleteproperty?id=${propertyview.id }">Delete</a>
				</td> -->
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>