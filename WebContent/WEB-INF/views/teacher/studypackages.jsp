<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Contact Manager Home</title>
</head>
<body>
	<div align="center">
		<table>
			<tr>
			<td>
			<input onclick="update()" class="button" type="submit" form="form1" value="Home" style="width:10em"/>
	        </td>
	        <td>
	        <input onclick="update()" class="button" type="submit" form="form3" value="New StudyPackage" style="width:10em"/>
			</td>
		</table>
	</div>
	<div id="Progress_Status"> 
		<h1 align="center">StudyPackages List</h1>		
		<h2 align="center">Teacher: ${teacher.name}</h2>
	  <div id="myprogressBar"></div> 
	</div>
	<div align="center">
	    <!--  
		<a href="index">Home</a>
		-->
		<form:form action="teachergohome3" method="get" modelAttribute="teacher" id="form1">
			<form:hidden path="userid"/>	 		
	   		<!-- <input type="submit" value="Home"/> -->	
		</form:form>
		
		<form:form action="newstudypackage" method="get" modelAttribute="teacher" style="margin-top:40px" id="form3">
			<form:hidden path="id"/>
	   		<!--  <input type="submit" value="New StudyPackage"/>-->	
		</form:form>		


		

		
		<!--
		<h3><a href="newstudypackage">New StudyPackage</a> </h3>
		  -->
		  
		<table border="1" cellpadding="5" style="margin-top:10px;">
			<tr>
				<th>No</th>
				<th>Subject</th>
				<th>Package Type</th>
				<!-- <th>Teacher</th> -->
				<th>Price<br> (E/m)</th>
				<th>Available</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listStudyPackageView}" var="studypackageview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<td>${studypackageview.subname}</td>
				<td>${studypackageview.packname}</td>
				<!--  <td>${studypackageview.name}</td>-->
				<td>${studypackageview.price}</td>
				<td>${studypackageview.available}</td>
				<td>
					<a onclick="update()" href="editstudypackage?id=${studypackageview.id }">Edit</a>
					&nbsp;&nbsp;
					<a onclick="update()" href="deletestudypackage?id=${studypackageview.id }">Delete</a>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>