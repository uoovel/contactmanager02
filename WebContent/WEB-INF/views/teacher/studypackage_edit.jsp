<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>New/Edit StudyPackage</title>
</head>
<body>
<div id="Progress_Status"> 
	<h1 align="center">New/Edit StudyPackage</h1>
	<div id="myprogressBar"></div> 
</div>
	<div align="center">
		
		<form:form action="savestudypackage" method="post" modelAttribute="studypackage">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>
			<tr>
				<td>Subject:</td>
				<!--  <td><form:input path="subjectid" value="${subject.subname}"/></td>-->
				<td>
					<form:select path="subjectid">						
						<form:option value="${subject.id}" label="${subject.subname}"/>
						<form:options items="${listSubject}" 
						itemValue="id" itemLabel="subname"/>	
					</form:select>
				</td>
			</tr>
			<tr>
				<td>PackageType:</td>
				<td>
					<form:select path="packagetypeid">						
						<form:option value="${packagetype.id}" label="${packagetype.packname}"/>
						<form:options items="${listPackagetype}" 
						itemValue="id" itemLabel="packname"/>	
					</form:select>
				</td>				
				
			</tr>
			<tr>
				<td>Teacher:</td>
				<td>
					<form:select path="teacherid">						
						<form:option value="${teacher.id}" label="${teacher.name}"/>
						<!--<form:options items="${listTeacher}" 
						itemValue="id" itemLabel="name"/>	-->
					</form:select>				
				<td/>
			</tr>
			<tr>
				<td>Price (E/m):</td>
				<td><form:input path="price"/></td>
			</tr>
			<tr>
				<td>Available:</td>
				<td><form:input path="available"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input onclick="update()" type="submit" value="SaveStudyPackage"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>