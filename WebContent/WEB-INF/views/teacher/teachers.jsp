<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>      
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Teachers Home</title>
</head>
<body>

	<div align="center">
		<form:form action="teachergohome3" method="get" modelAttribute="teacher" id="form1">
			<form:hidden path="userid"/>	
		</form:form>
		<form:form action="index" method="get" id="form2">				
		</form:form>
		<table>
			<tr>
			<td>
			<input onclick="update()" class="button" type="submit" form="form1" value="Home" style="width:10em"/>
	        </td>
	        <td>
	        <input onclick="update()" class="button" type="submit" form="form2" value="Log out" style="width:10em"/>
			</td>
		</table>
	</div>
	<div id="Progress_Status"> 
		<h1 align="center">My personal details, TEACHER</h1>
		<div id="myprogressBar"></div> 
	</div>	
		


	<div align="center">
		<!-- <h3><a href="newstudent">New Student</a> </h3> -->
		<table border="1" cellpadding="5" style="margin-top:40px;">
			<tr>
				<th>No</th>
				<th>Name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listTeacher}" var="teacher" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${teacher.name}</td>
				<td>${teacher.email}</td>
				<td>
					<a onclick="update()" href="editteacher?id=${teacher.id }">Edit</a>
					&nbsp;&nbsp;
					<!--  <a href="deletestudent?id=${student.id }">Delete</a> -->
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>