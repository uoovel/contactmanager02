<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>         
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Ratings Home</title>
</head>
<body>
<div id="Progress_Status"> 
	<h1>Ratings List</h1>
    <div id="myprogressBar"></div> 
</div>
	<div align="center">
	    <!-- 
		<a href="index">Home</a>
		 -->
		
		<h2>Student: ${student.studname} &nbsp;
		<a onclick="update()" href="newrating?studentid=${student.id}&teacherid=${teacher.id}">New Rating</a>
		</h2>
		<!-- <h3><a href="newrating">New Rating</a> </h3> -->
		<!--<form:form action="newrating" method="get" modelAttribute="teacher">
			<form:hidden path="id"/>	 		
	   		<input type="submit" value="New Rating"/>	
		</form:form>-->
		
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<!-- <th>Student</th> -->
				<th>Criteria</th>
				<th>Score</th>

				<!--  <th>Action</th>-->
			</tr>
			<c:forEach items="${listRatingView}" var="ratingview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				
				<!-- <td>${ratingview.studname}</td> -->
				<td>${ratingview.ecritname}</td>
				<td>${ratingview.score}</td>
                
				<!--<td>
					<a href="newrating?studentid=${ratingview.studentid}&teacherid=${ratingview.teacherid}">New Rating</a>
				</td>  -->
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>