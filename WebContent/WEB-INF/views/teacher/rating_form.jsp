<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>         
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>

<title>New/Edit Rating</title>
</head>
<body>
<div id="Progress_Status"> 
	<h1>New/Edit Rating</h1>
    <div id="myprogressBar"></div> 
</div>
	<div align="center">
		
		<form:form action="saverating" method="post" modelAttribute="rating">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>
			<tr>
				<td>Student:</td>
				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>
						<!--<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>	-->
					</form:select>
				</td>
			</tr>
			<tr>
				<td>Criteria:</td>
				<td>
					<form:select path="ecriteriaid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listEcriteria}" 
						itemValue="id" itemLabel="ecritname"/>	
					</form:select>
				</td>				
			</tr>

			<tr>
				<td>Score:</td>
				<td><form:input path="score" type="RANGE" MIN="0" MAX="99" STEP="1" VALUE="50"
				class="slider" id="myRange"/></td>
				<td>
				  <p>Value: <span id="demo"></span></p>
				  </td>
			</tr>
			<tr>
				<td>Teacher:</td>
				<td>
					<form:select path="teacherid">						
						<form:option value="${teacher.id}" label="${teacher.name}"/>
						<!--<form:options items="${listTeacher}" 
						itemValue="id" itemLabel="name"/>	-->
					</form:select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center"><input onclick="update()" type="submit" value="SaveRating"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>
<!--  https://www.w3schools.com/howto/howto_js_rangeslider.asp-->
<script>
var slider = document.getElementById("myRange");
var output = document.getElementById("demo");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function() {
  output.innerHTML = this.value;
}

</script>
</body>
</html>