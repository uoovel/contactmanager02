<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>   
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Insert title here</title>
</head>
<body>
<div align="center">
<a onclick="update()" href="index">Log out</a>
</div>

<div id="Progress_Status"> 
	<h1 align="center">Hello ${teacher.name}, </h1>
	<h2 align="center">You have logged in as teacher</h2>
	<div id="myprogressBar"></div> 
</div> 
	
<div align="center">
		<form:form action="teachers" method="get" modelAttribute="teacher">
		<table cellpadding="$">			 
			<form:hidden path="userid"/>			 
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="My Details"/></td>			
			</tr>			 
		</table>		
		</form:form>
		<form:form action="studypackages" method="get" modelAttribute="teacher">
		<table cellpadding="$">			 
			<form:hidden path="userid"/>			 
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="My Study Packages"/></td>			
			</tr>			 
		</table>		
		</form:form>		
		
		
		<form:form action="teacherstudentpacks" method="get" modelAttribute="teacher" style="margin-top:40px;">
   			<form:hidden path="userid"/>   		
	    	<input onclick="update()" type="submit" value="Registered Students"/>	
   		</form:form>
		
		
		<!-- 
		<a href="studypackages">Study Packages</a>	
		     -->
	   <!-- 
	    <h3>
    		<a href="students">Students</a>
    	</h3>
	    -->

	    <!--
	    <a href="studentpacks">My Courses</a>
	      -->
	      
</div>
<!--
	<script>

	function update() { 
	
		  var element = document.getElementById("myprogressBar");    
		  var width = 1; 
		  var identity = setInterval(scene, 10); 
		  //var marginleft= 1;
		  
		  function scene() {
			if(width >=30){
				sleep(300);
			}
		    if (width >= 100) { 
		      width = 1;
		      //clearInterval(identity); 
		    } else { 
		      width++;
		      //marginleft++;
		      element.style.width = width + '%'; 
		      //element.style.margin-left = marginleft + '%'; 
		      
		    } 
		  }
 		  
		
	}
	function sleep(milliseconds) {
		  var start = new Date().getTime();
		  for (var i = 0; i < 1e7; i++) {
		    if ((new Date().getTime() - start) > milliseconds){
		      break;
		    }
		  }
		}

	</script>	  -->      
</body>
</html>