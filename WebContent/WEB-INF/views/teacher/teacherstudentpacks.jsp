<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>       
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>Students In Packs</title>
</head>
<body>
	<div align="center">
		<table>
			<tr>
			<td>
			<input onclick="update()" class="button" type="submit" form="form1" value="Home" style="width:10em"/>
	        </td>
	        <td>
	        <input onclick="update()" class="button" type="submit" form="form2" value="Log out" style="width:10em"/>
			</td>
		</table>
	</div>
	<div id="Progress_Status"> 
		<h1 align="center">Registered Students</h1>
	    <div id="myprogressBar"></div> 
	</div> 

	<div align="center">
   		<form:form action="teachergohome3" method="get" modelAttribute="teacher" id="form1">
			<form:hidden path="userid"/>	
		</form:form>
		<form:form action="index" method="get" id="form2">				
		</form:form>


		

		
		<table border="1" cellpadding="5" style="margin-top:40px">
			<tr>
				<th>No</th>
				<th>Study Package</th>			
				<th>Student(score)</th>
				<th>Begin</th>
				<th>Action</th>				
			</tr>
			<c:forEach items="${listStudentPackView}" var="studentpackview" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>				
				<td>${studentpackview.studypackageview}</td>
				<td>${studentpackview.studname}<br>(${studentpackview.score})</td>
				<td>${studentpackview.begin}</td>
				<td>
				    <div>
					<a onclick="update()" href="teditstudentpack?id=${studentpackview.id }">Edit</a>
					&nbsp;&nbsp;
					</div>
					<br>
					<div style="margin-top:5px">
					<a onclick="update()" href="tdeletestudentpack?id=${studentpackview.id }">Delete</a>
					&nbsp;&nbsp;
					</div>
					<br>
					<div style="margin-top:5px">
					<a onclick="update()" href="tviewprofile?id=${studentpackview.id }" >View Profile</a>
				    </div>
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>

	
</body>
</html>