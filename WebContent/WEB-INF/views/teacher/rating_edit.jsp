<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>New/Edit Rating</title>
</head>
<body>
	<div align="center">
		<h1>New/Edit Rating</h1>
		<form:form action="saverating" method="post" modelAttribute="rating">
		<table cellpadding="$">
			<form:hidden path="id"/>
			<tr>
				<td>StudentId:</td>

				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>
						<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>	
					</form:select>
				</td>
			</tr>
			<tr>
				<td>EcriteriaId:</td>
				<td>
					<form:select path="ecriteriaid">						
						<form:option value="${ecriteria.id}" label="${ecriteria.ecritname}"/>
						<form:options items="${listEcriteria}" 
						itemValue="id" itemLabel="ecritname"/>	
					</form:select>
				</td>				
				
			</tr>

			<tr>
				<td>Score:</td>
				<td><form:input path="score"/></td>
			</tr>

			<tr>
				<td colspan="2" align="center"><input type="submit" value="SaveRating"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>