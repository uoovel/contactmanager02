<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
<title>Students Home</title>
</head>
<body>
	<div align="center">
		<a href="index">Log out</a>
		<h1>My personal details</h1>
		<!-- <h3><a href="newstudent">New Student</a> </h3> -->
		<table border="1" cellpadding="5">
			<tr>
				<th>No</th>
				<th>Student Name</th>
				<th>Email</th>
				<th>Action</th>
			</tr>
			<c:forEach items="${listStudent}" var="student" varStatus="status">
			<tr>
				<td>${status.index + 1}</td>
				<td>${student.studname}</td>
				<td>${student.email}</td>
				<td>
					<a href="editstudent?id=${student.id }">Edit</a>
					&nbsp;&nbsp;
					<!--  <a href="deletestudent?id=${student.id }">Delete</a> -->
				</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>