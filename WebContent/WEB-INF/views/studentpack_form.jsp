<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>    
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>New/Edit StudentPack</title>
</head>
<body>

	<div align="center">
		<form:form action="gohome2" method="get" modelAttribute="student" id="form1">
			<form:hidden path="userid"/>	
		</form:form>
		<form:form action="index" method="get" id="form2">				
		</form:form>
		<table>
			<tr>
			<td>
			<input onclick="update()" class="button" type="submit" form="form1" value="Home" style="width:10em"/>
	        </td>
	        <td>
	        <input onclick="update()" class="button" type="submit" form="form2" value="Log out" style="width:10em"/>
			</td>
		</table>
	</div>
	<div id="Progress_Status"> 	  
	  <h1 align="center">New/Edit StudentPack</h1>
	  <div id="myprogressBar"></div> 
	</div>	
	
		
		
		
	<div align="center">
		<!-- 	<form:form action="gohome2" method="get" modelAttribute="student">
			<form:hidden path="userid"/>	 		
	   		<input type="submit" value="Home"/>	
		</form:form> -->	
		<br>
		<form:form action="savestudentpack" method="post" modelAttribute="studentpack">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>
			<tr>
				<td>StudyPackage:</td>
				<td>
					<form:select path="studypackageid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listStudyPackageView}" 
						itemValue="id" itemLabel="studypackagename"/>	
					</form:select>
				</td>
			</tr>

			<tr>
				<td>Student:</td>
				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>

					</form:select>				
				<td/>
			</tr>

			<tr>
				<td>Begin ('YYYY-MM-DD'):</td>
				<td><form:input path="begin" value="2020-10-30"/></td>
			</tr>
			<tr>
				<td colspan="2" align="center">
				<input onclick="update()" type="submit" value="SaveStudentPack"/></td>
			
			</tr>
		</table>		
		</form:form>
	
	</div>

</body>
</html>