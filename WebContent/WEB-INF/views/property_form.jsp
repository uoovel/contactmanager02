<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>      
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
    <script src="<s:url value="/static/js/myscript.js" />"></script>
<title>New/Edit Property</title>
</head>
<body>
<div id="Progress_Status"> 
  
   
  <h1 align="center">New/Edit Property</h1>
</div>
	<div align="center">
		
		<form:form action="saveproperty" method="post" modelAttribute="property" id="form1">
		<table cellpadding="$" class="gfg">
			<form:hidden path="id"/>			
			<tr>
				<td>Student:</td>
				<td>
					<form:select path="studentid">						
						<form:option value="${student.id}" label="${student.studname}"/>
						<!--<form:options items="${listStudent}" 
						itemValue="id" itemLabel="studname"/>-->	
					</form:select>
				</td>
			</tr>
			<tr style="margin-top:15px;">
				<td>Criteria:</td>
				<td>
					<form:select path="criteriaid">						
						<form:option value="-" label="--Please Select"/>
						<form:options items="${listCriteria}" 
						itemValue="id" itemLabel="critname"/>	
					</form:select>
				</td>				
			</tr>

			<tr>
				<div>
					<td>Description:</td>
				</div>
			</tr>
			<tr>
				<td colspan="2" align="center"><form:textarea path="description" style="height:150px"/></td>
			</tr>

			<!-- <tr>
				<td colspan="2" align="center">
		
			    
			</tr> -->
		</table>		
		</form:form>

	</div>
	<div id="myprogressBar" style=margin-top:0px;></div>
	<div align="center" style=margin-top:0px;>			
		<input onclick="update()" type="submit" value="SaveProperty" form="form1"/></td>
	</div>
</body>
</html>