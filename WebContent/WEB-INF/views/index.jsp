<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<!-- <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" 
	name="viewport" content="width=device-width, initial-scale=1.0">
	 -->
	<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="<s:url value="/static/js/myscript.js" />"></script>
	<title>Students and teacher WebApp, Front Page</title>
	<s:url var="url_css" value="/static/css/style.css"/>
	<link href="${url_css}" rel="stylesheet" type="text/css"/>
    <link href="static/css/style.css"/>
</head>
<!--<s:url var="url_bg" value="/static/images/Eutech82.png"/>-->
<body>


<div align="center">

	<div align="center" style="margin-top:20px">
    	<a onclick="update()" href="loginn">Login</a>          
    	<a onclick="update()" href="register" style="padding:25px;">Register</a>
    </div>
    <table align="center" style="margin-top:20px">
    <th align="center">What can You do in this site</th>    
    	<tr><td>-Get assistance in studies</td></tr>   
    	<tr><td>-Improve Your results at school</td></tr>
    	<tr><td>-View teachers profiles</td></tr>
    	<tr><td>-Enrol to courses</td></tr>
    </table>  
</div>    

<div id="Progress_Status" style="background-color:lightblue">
  <h1 align="center">Student & Teacher</h1>
  <h2 align="center">Everyone can boost the results in studies</h2>
  <div id="myprogressBar"></div> 
</div>    
        
<div align="center">
    <table align="center" style="margin-top:20px">
    <th align="center">Available courses</th>    
    	<tr><td>-Java basics</td></tr>   
    </table>  
       
    <table align="center" style="margin-top:20px">
    <th align="center">Instruction</th>
    <tr><td>-If you are first time, click "Register"</td></tr>
    <br>
    <tr><td>-Remember your email & password for "Login"</td></tr>
    <br>
    <tr><td>-Once you have registered, click "Login"</td></tr>    
    </table>
    
    
    
   	<div align="center" style="margin-top:20px">
    	<a onclick="update()" href="teacherjoyn">Teacher, joyn!</a>          
    	
    </div>
    
    
    
    <!--  <img src="Eutech82.png" alt="Forestphoto" width="500" height="600">
    -->
    <div align="center" style="margin-top:100px">
	    <!--  
	    <img 
	    src="${pageContext.request.contextPath}/resources/images/Eutech82.png"
	    />
	    <spring:url value="/resources/images" var="images" />
	    <img src="${images}/Eutech82.png"/>
		
		<img src="<c:url value="/resources/images/Eutech82.png" />" alt="image" />
		-->
		<img src=
	    "<s:url value="/static/images/Eutech82.png"/>"
	    width=100% height=100%/>	
	</div>
</div>	

</body>
</html>