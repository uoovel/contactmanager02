package net.codejava.contact.dao;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.StudentPack;

//import net.codejava.contact.model.StudyPackage;

public class StudentPackDAOImpl implements StudentPackDAO{
	private JdbcTemplate jdbcTemplate;
	
	public StudentPackDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(StudentPack c) {
		String sql = "INSERT INTO studentpack (studypackage_id, student_id,"
				+ " begin) VALUES (?, ?, ?)";
		return jdbcTemplate.update(sql, c.getStudypackageid(), c.getStudentid(),
				c.getBegin());
		//return 0;
	}

	@Override
	public int update(StudentPack c) {
		String sql = "UPDATE studentpack SET studypackage_id=?, student_id=?,"
				+ "begin=? WHERE studentpack_id=?";		
		return jdbcTemplate.update(sql, c.getStudypackageid(), c.getStudentid(),
				c.getBegin(), c.getId());
	}

	@Override
	public StudentPack get(Integer id) {
		String sql = "SELECT * FROM studentpack WHERE studentpack_id=" + id;
		ResultSetExtractor<StudentPack> extractor = new ResultSetExtractor<StudentPack>() {

			@Override
			public StudentPack extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					//System.out.println();
					Integer id = rs.getInt("studentpack_id");
					Integer studypackageid = rs.getInt("studypackage_id");					
					Integer student_id = rs.getInt("student_id");					
					Date begin = rs.getDate("begin");
					return new StudentPack(id, studypackageid, student_id, 
							begin);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM studentpack WHERE studentpack_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<StudentPack> list() {
		String sql = "SELECT * FROM studentpack";
		RowMapper<StudentPack> rowMapper = new RowMapper<StudentPack>() {

			@Override
			public StudentPack mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("studentpack_id");
				Integer studypackageid = rs.getInt("studypackage_id");
				Integer studentid = rs.getInt("student_id");
				Date begin = rs.getDate("begin");
				return new StudentPack(id, studypackageid, studentid, begin);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
