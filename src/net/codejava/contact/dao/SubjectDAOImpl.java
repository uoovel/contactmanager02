package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.Subject;

public class SubjectDAOImpl implements SubjectDAO {

	private JdbcTemplate jdbcTemplate;
	
	public SubjectDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(Subject c) {
		String sql = "INSERT INTO subject (subname) VALUES (?)";
		return jdbcTemplate.update(sql, c.getSubname());
		//return 0;
	}

	@Override
	public int update(Subject c) {
		System.out.println("Kontr505: SubjectDAOImpl/update");
		String sql = "UPDATE subject SET subname=? WHERE subject_id=?";
		System.out.println("Kontr510: SubjectDAOImpl/update" + c);
		System.out.println("Kontr515: SubjectDAOImpl/update" + c.getSubname());
		return jdbcTemplate.update(sql, c.getSubname(), c.getId());
	}

	@Override
	public Subject get(Integer id) {
		String sql = "SELECT * FROM subject WHERE subject_id=" + id;
		ResultSetExtractor<Subject> extractor = new ResultSetExtractor<Subject>() {

			@Override
			public Subject extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					String subname = rs.getString("subname");				
					return new Subject(id, subname);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM subject WHERE subject_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<Subject> list() {
		String sql = "SELECT * FROM subject";
		RowMapper<Subject> rowMapper = new RowMapper<Subject>() {

			@Override
			public Subject mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("subject_id");
				String subname = rs.getString("subname");
				return new Subject(id, subname);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}

}
