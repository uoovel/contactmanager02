package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Property;

//import net.codejava.contact.model.StudentPack;

public interface PropertyDAO {
	public int save(Property property);
	
	public int update(Property property);
	
	public Property get(Integer id);
	
	public int delete(Integer id);
	
	public List<Property> list();
}
