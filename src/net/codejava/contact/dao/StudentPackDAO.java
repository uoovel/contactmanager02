package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.StudentPack;


public interface StudentPackDAO {
	public int save(StudentPack studentpack);
	
	public int update(StudentPack studentpack);
	
	public StudentPack get(Integer id);
	
	public int delete(Integer id);
	
	public List<StudentPack> list();
}
