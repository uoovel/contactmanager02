package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Student;

public interface StudentDAO {
	public int save(Student student);
	
	public int update(Student student);
	
	public Student get(Integer userid);
	
	public int delete(Integer id);
	
	public List<Student> list(Integer id);
	
	public List<Student> list();

	public Student get(Integer id, String edit);

	List<Student> listByid(Integer id);
}
