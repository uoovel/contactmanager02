package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Student;

public interface ContactDAO {
	public int save(Contact contact);
	
	public int update(Contact contact);
	
	public Contact get(Integer userid);
	
	public int delete(Integer id);
	
	public List<Contact> list(Integer userid);
	
	public List<Contact> list(Integer id, String string);
	
	public List<Contact> list();
	
	public Contact get(Integer id, String edit);

}


