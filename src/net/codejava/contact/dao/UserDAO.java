package net.codejava.contact.dao;

import java.util.List;

import net.codejava.contact.model.User;

//import net.codejava.contact.model.Student;

public interface UserDAO {
	public int save(User user);
	
	public int update(User user);
	
	public User get(Integer id);
	
	public int delete(Integer id);
	
	public List<User> list();
}
