package net.codejava.contact.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import net.codejava.contact.model.StudyPackage;

//import net.codejava.contact.model.StudyPackage;



public class StudyPackageDAOImpl implements StudyPackageDAO{
	private JdbcTemplate jdbcTemplate;
	
	public StudyPackageDAOImpl(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	@Override
	public int save(StudyPackage c) {
		String sql = "INSERT INTO studypackage (subject_id, packagetype_id,"
				+ " teacher_id, price, available) VALUES (?, ?, ?, ?, ?)";
		return jdbcTemplate.update(sql, c.getSubjectid(), c.getPackagetypeid(),
				c.getTeacherid(), c.getPrice(), c.getAvailable());
		//return 0;
	}

	@Override
	public int update(StudyPackage c) {
		String sql = "UPDATE studypackage SET subject_id=?, packagetype_id=?,"
				+ "teacher_id=?, price=?, available=? WHERE studypackage_id=?";		
		return jdbcTemplate.update(sql, c.getSubjectid(), c.getPackagetypeid(),
				c.getTeacherid(), c.getPrice(), c.getAvailable(), c.getId());
	}

	@Override
	public StudyPackage get(Integer id) {
		String sql = "SELECT * FROM studypackage WHERE studypackage_id=" + id;
		ResultSetExtractor<StudyPackage> extractor = new ResultSetExtractor<StudyPackage>() {

			@Override
			public StudyPackage extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					//System.out.println();
					Integer id = rs.getInt("studypackage_id");
					Integer subjectid = rs.getInt("subject_id");					
					Integer packagetype_id = rs.getInt("packagetype_id");
					Integer teacher_id = rs.getInt("teacher_id");				
					double price = rs.getDouble("price");
					boolean available = rs.getBoolean("available");
					return new StudyPackage(id, subjectid, packagetype_id, 
							teacher_id, price, available);
				}
				return null;
			}
			
		};
		
		return jdbcTemplate.query(sql, extractor);
	}

	@Override
	public int delete(Integer id) {
		String sql = "DELETE FROM studypackage WHERE studypackage_id=" + id;
		return jdbcTemplate.update(sql);
	}

	@Override
	public List<StudyPackage> list() {
		String sql = "SELECT * FROM studypackage";
		RowMapper<StudyPackage> rowMapper = new RowMapper<StudyPackage>() {

			@Override
			public StudyPackage mapRow(ResultSet rs, int rowNum) throws SQLException {
				Integer id = rs.getInt("studypackage_id");
				Integer subjectid = rs.getInt("subject_id");
				Integer packagetype_id = rs.getInt("packagetype_id");
				Integer teacher_id = rs.getInt("teacher_id");				
				double price = rs.getDouble("price");
				boolean available = rs.getBoolean("available");
				return new StudyPackage(id, subjectid, packagetype_id, teacher_id, price, available);
			}
			
		};		
		return jdbcTemplate.query(sql, rowMapper);		
	}
}
