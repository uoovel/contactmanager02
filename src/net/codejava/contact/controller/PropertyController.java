package net.codejava.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.CriteriaDAO;
import net.codejava.contact.dao.PropertyDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.model.Criteria;
import net.codejava.contact.model.Property;
import net.codejava.contact.model.PropertyView;
import net.codejava.contact.model.Student;

//import net.codejava.contact.dao.ContactDAO;
//import net.codejava.contact.dao.PackageTypeDAO;
//import net.codejava.contact.dao.StudyPackageDAO;
//import net.codejava.contact.dao.SubjectDAO;
//import net.codejava.contact.model.Contact;
//import net.codejava.contact.model.PackageType;
//import net.codejava.contact.model.StudyPackage;
//import net.codejava.contact.model.StudyPackageView;
//import net.codejava.contact.model.Subject;

@Controller
public class PropertyController {
	@Autowired
	private PropertyDAO propertyDAO;
	
	@Autowired
	private StudentDAO studentDAO;
	
	@Autowired
	private CriteriaDAO criteriaDAO;	
	 
	//Integer userid;
	//@Autowired
	//private ContactDAO contactDAO;
	
	//PackageTypes
	/*
	@RequestMapping(value = "/administration/properties")
	public ModelAndView listProperty(ModelAndView model) {
		//properties
		List<Property> listProperty = propertyDAO.list();
		//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
		model.addObject("listProperty", listProperty); //the left argument is correspondent to field in .jsp
		//////////
		List<PropertyView> listPropertyView = new ArrayList<>();
		
		for (int i = 0; i < listProperty.size(); i++){		
			
			Property property = listProperty.get(i);
			
			Integer id = property.getId();//*
			
			Integer studentid = property.getStudentid();						
			Student student = studentDAO.get(studentid);
			String studname = student.getStudname();//*
			
			Integer criteriaid = property.getCriteriaid();						
			Criteria criteria = criteriaDAO.get(criteriaid);
			String critname = criteria.getCritname();//*
			
			//Integer teacherid = studypackage.getTeacherid();						
			//Contact teacher = contactDAO.get(teacherid);
			//String name = teacher.getName();//*
			
			String description = property.getDescription();
			//boolean available = studypackage.getAvailable();
			
			PropertyView propertyview = new PropertyView(
					id, studname, critname, description);
			listPropertyView.add(propertyview);
			
		};//for
		
		//System.out.println(listStudyPackageView);
		
		
		model.addObject("listPropertyView", listPropertyView);
		
		
		
		
		///////////
		model.setViewName("administration/properties");
		
		return model;
	}
	*/	
	@RequestMapping(value = "/newproperty", method = RequestMethod.GET)
	public ModelAndView newProperty(ModelAndView model, @ModelAttribute Student student) {
		//List<Integer> listSubject = new ArrayList<Integer>();//my
		//listSubject.add(2);//my
		Integer userid = student.getUserid();
		//System.out.println("ProprtyController: " + student);
		List<Student> listStudent = studentDAO.list(userid); //my		
		model.addObject("listStudent", listStudent);//my
		List<Criteria> listCriteria = criteriaDAO.list(); //my		
		model.addObject("listCriteria", listCriteria);//my		
		//List<Contact> listTeacher = contactDAO.list(); //my		
		//model.addObject("listTeacher", listTeacher);//my			
		Student student2 = studentDAO.get(userid);
		model.addObject("student", student2);
		Property newProperty = new Property();
		model.addObject("property", newProperty);
		
		
		model.setViewName("property_form");	
		//System.out.println("Kont600: StudyPackageController /newstudypackage: ");
		return model;
		
	}
	@RequestMapping(value = "/saveproperty", method = RequestMethod.POST)
	public ModelAndView saveProperty(ModelAndView model, @ModelAttribute Property property){
		//System.out.println("Kont700: StudyPackageController" + studypackage);
		
		if (property.getId() == null) {
			propertyDAO.save(property);
		}else {
			
			propertyDAO.update(property);
			
		}
		Integer studentid = property.getStudentid();
		Student student = studentDAO.get(studentid, "string");
		
		model.addObject("student", student);
		model.setViewName("/welcome");
		return model;
	}
	
	@RequestMapping(value = "/editproperty", method = RequestMethod.GET)
	public ModelAndView editProperty(HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));		
		
		Property property = propertyDAO.get(id);
		//System.out.println("Kont800: MainController: " + request.getParameter("id"));
		//System.out.println("Kont805: MainController: " + studypackage);
		
		ModelAndView model = new ModelAndView("property_edit");	
		
		
		model.addObject("property", property);
		
		
		Integer studentid = property.getStudentid(); //my
		//System.out.println("Kont810: MainController: " + subjectid);
		Student student = studentDAO.get(studentid, "string"); //my		
		//userid = student.getUserid();
		model.addObject("student", student);//my
		Integer userid = student.getUserid();
		List<Student> listStudent = studentDAO.list(userid); //my		
		model.addObject("listStudent", listStudent);//my
		
		
		Integer criteriaid = property.getCriteriaid(); //my		
		Criteria criteria = criteriaDAO.get(criteriaid); //my		
		model.addObject("criteria", criteria);//my		
		List<Criteria> listCriteria = criteriaDAO.list(); //my		
		model.addObject("listCriteria", listCriteria);//my		
		
		//Integer teacherid = studypackage.getTeacherid(); //my		
		//Contact teacher = contactDAO.get(teacherid); //my		
		//model.addObject("teacher", teacher);//my		
		//List<Contact> listTeacher = contactDAO.list(); //my		
		//model.addObject("listTeacher", listTeacher);//my		
	
		
		
		//System.out.println("Kont815: MainController: " + subject);
		
		return model;
	}
	
	@RequestMapping(value = "/deleteproperty", method = RequestMethod.GET)
	public ModelAndView deleteProperty(ModelAndView model, @RequestParam Integer id){
		Property property = propertyDAO.get(id);
		Integer studentid = property.getStudentid();
		Student student = studentDAO.get(studentid, "string");
		propertyDAO.delete(id);
		
		model.addObject("student", student);
		model.setViewName("/welcome");		
		return model;
	}
}
