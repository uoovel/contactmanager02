package net.codejava.contact.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.UserDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.User;





@Controller
public class MainController {
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private StudentDAO studentDAO;
	
	//Integer userid;
	
	@RequestMapping(value = "/users")
	public ModelAndView listStudent(ModelAndView model) {
		//System.out.println("Control100: Controller");
		////List<User> listUser = userDAO.list();
		//System.out.println("Control200: Controller");
		//model.addObject("listUser", listUser);
		//System.out.println("Control300: Controller");
		model.setViewName("users");	
		//System.out.println("Control400: Controller");
		return model;
	}
	
	
    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView visitHome() {
        return new ModelAndView("index");
    }
    @RequestMapping(value="/loginn", method = RequestMethod.GET)
    public ModelAndView loginn(ModelAndView model) {
    	User newUser = new User();
    	model.addObject("user", newUser);
    	model.setViewName("login_form");
        return model;
    }
    
    @RequestMapping(value="/register", method = RequestMethod.GET)
    public ModelAndView register(ModelAndView model) {
    	User newUser = new User();
    	model.addObject("user", newUser);
    	model.setViewName("register_form");
        return model;
    }

	@RequestMapping(value = "/loginUser", method = RequestMethod.POST)
	public ModelAndView saveStudent(@ModelAttribute User user){
		String password = user.getPassword();
		String encryptedPass = CryptPass.encrypt("JavasEncryptDemo", "RandomJavaVector", password);
		user.setPassword(encryptedPass);
		
		
		List<User> listUser = userDAO.list();
		System.out.println(listUser);
		//userid = user.getId();
		//otsib listist kuni leiab	
		String userEmail = user.getEmail();
		boolean autenditud1 = false;
		boolean autenditud2 = false;
		User userr = new User();
		for(int i = 0; i<listUser.size(); i++) {
			userr = listUser.get(i);
			String email = userr.getEmail();
			if(userEmail.equals(email)) {
				autenditud1 = true;
				break;
			}			
		}
		if(autenditud1 == true) {
			if ((user.getPassword()).equals(userr.getPassword())) {
				autenditud2 = true;
			}
		}
		
		
		//System.out.println("Kontr500: MainController: " + user.getEmail());
		Student student = new Student();
		if (autenditud2 == true) { //user.getEmail().equals("i")
			ModelAndView model = new ModelAndView();
			//model.addObject("user", userr);

	    	//if first login, then save to student
	    	if(userr.getCounter() == 0) {
	    		student.setStudname(userr.getUname());
	    		student.setEmail(userr.getEmail());
	    		student.setUserid(userr.getId());
	    		//student = new Student(userr.getUname(), userr.getEmail(),
	    				//userr.getId());
	    		studentDAO.save(student);
	    		
	    	}
	    	//update user, because counter should rise
	    	userr.setCounter(userr.getCounter()+1);
	    	userDAO.update(userr);  	
			
	    	student = studentDAO.get(userr.getId());
			if(student==null)
	    	{
				student = new Student();
	    		student.setStudname(userr.getUname());
	    		student.setEmail(userr.getEmail());
	    		student.setUserid(userr.getId());
	    		studentDAO.save(student);
			}
			
			//System.out.println("Kontr600: MainController, student: " + student);
			model.addObject("student", student);
			model.setViewName("welcome");
	    	
			return model;
		} else {
			return new ModelAndView("redirect:/bad_trial");
		}
	}
	
    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public ModelAndView welcome() {
    	//System.out.println("MainController,welcome");
    	//ModelAndView model = new ModelAndView();
    	//Student student = new Student();    	
    	//student = studentDAO.get(userid);
    	//model.addObject("student", student);
		//model.setViewName("welcome");
    	//return model;
        return new ModelAndView("welcome");
    }	

    @RequestMapping(value="/bad_trial", method = RequestMethod.GET)
    public ModelAndView badTrial() {
        return new ModelAndView("bad_trial");
    }
    //registering method
	@RequestMapping(value = "/registerstudent", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute User user){
		List<User> listUser = userDAO.list();
		String password = user.getPassword();
		String encryptedPass = CryptPass.encrypt("JavasEncryptDemo", "RandomJavaVector", password);
		user.setPassword(encryptedPass);
		//System.out.println("registerstudent: " + password);
		//System.out.println("registerstudent: " + encryptedPass);
		boolean exists1 = false;
		String userEmail = user.getEmail();
		//boolean autenditud2 = false;
		User userr = new User();
		for(int i = 0; i<listUser.size(); i++) {
			userr = listUser.get(i);
			String email = userr.getEmail();
			if(userEmail.equals(email)) {
				exists1 = true;
				break;
			}			
		}
		
		if(exists1 != true) {
			if (user.getId() == null) {
				userDAO.save(user);
			}else {
				//System.out.println("Kont200: MainController" + contact);
				userDAO.update(user);
			}		
			return new ModelAndView("redirect:/loginn");
		} else {
			return new ModelAndView("redirect:/exists");
		}
	}
    @RequestMapping(value="/exists", method = RequestMethod.GET)
    public ModelAndView exists() {
        return new ModelAndView("exists");
    }
	@RequestMapping(value ="/index")
	public ModelAndView frontPage(ModelAndView model) {
		//System.out.println("MainController, /index: ");
		model.setViewName("index");
		return model;
	}
    @RequestMapping(value="/toadministration", method = RequestMethod.GET)
    public ModelAndView admin() {
    	//!Not working
        return new ModelAndView("/toadministration");
    }    

    
    
}
