package net.codejava.contact.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.UserDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.User;

@Controller
public class TeacherController {
	
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private ContactDAO contactDAO;
	
	//Integer userid2;
	
	@RequestMapping(value = "/teacherloginUser", method = RequestMethod.POST)
	public ModelAndView saveTeacher(@ModelAttribute User user){
		//System.out.println("teacherloginUser: " + user);
		String password = user.getPassword();
		String encryptedPass = CryptPass.encrypt("JavasEncryptDemo", "RandomJavaVector", password);
		user.setPassword(encryptedPass);
		
		
		List<User> listUser = userDAO.list();
		
		//userid = user.getId();
		//otsib listist kuni leiab	
		String userEmail = user.getEmail();
		boolean autenditud1 = false;
		boolean autenditud2 = false;
		User userr = new User();
		for(int i = 0; i<listUser.size(); i++) {
			userr = listUser.get(i);
			String email = userr.getEmail();
			if(userEmail.equals(email)) {
				autenditud1 = true;
				break;
			}			
		}
		if(autenditud1 == true) {
			if ((user.getPassword()).equals(userr.getPassword())) {
				autenditud2 = true;
			}
		}
		
		
		//System.out.println("Kontr500: MainController: " + user.getEmail());
		Contact teacher = new Contact();
		if (autenditud2 == true) { //user.getEmail().equals("i")
			ModelAndView model = new ModelAndView();
			//model.addObject("user", userr);

	    	//if first login, then save to student
	    	if(userr.getCounter() == 0) {
	    		teacher.setName(userr.getUname());
	    		teacher.setEmail(userr.getEmail());
	    		teacher.setUserid(userr.getId());
	    		teacher.setAddress("address");
	    		teacher.setPhone("phone");
	    		
	    		//student = new Student(userr.getUname(), userr.getEmail(),
	    				//userr.getId());
	    		contactDAO.save(teacher);
	    		
	    	}
	    	//update user, because counter should rise
	    	userr.setCounter(userr.getCounter()+1);
	    	userDAO.update(userr);
	    	//System.out.println("teacherloginUser, userr: " + userr);
			teacher = contactDAO.get(userr.getId());
			if(teacher==null)
			{
				teacher = new Contact();
	    		teacher.setName(userr.getUname());
	    		teacher.setEmail(userr.getEmail());
	    		teacher.setUserid(userr.getId());
	    		teacher.setAddress("address");
	    		teacher.setPhone("phone");
	    		contactDAO.save(teacher);				
			}
			//System.out.println("Kontr600: MainController, student: " + student);
			model.addObject("teacher", teacher);
			//System.out.println("teacherloginUser: " + teacher);
			model.setViewName("/teacher/teacherwelcome");
	    	//userid2 = teacher.getUserid();
			return model;
		} else {
			return new ModelAndView("redirect:/bad_trial");
		}
	}
	
	@RequestMapping(value = "/teachers", method = RequestMethod.GET)
	public ModelAndView listTeacher(ModelAndView model, @ModelAttribute Contact teacher) {
		//System.out.println("Control102: Controller" + student);
		Integer userid = teacher.getUserid();
		//userid2 = userid;
		
		//if(id==null) {
		//	id = student.getUserid();
		//}
		//id = student.getUserid();
		//id = user.getId();
		List<Contact> listTeacher = contactDAO.list(userid);
		//List<Student> listStudent = studentDAO.list(1); //siia user_id parsida
		//System.out.println("Control200: Controller" + listTeacher);
		model.addObject("teacher", teacher);
		//System.out.println("teachers: " + teacher);
		model.addObject("listTeacher", listTeacher);
		
		model.setViewName("/teacher/teachers");	
		//System.out.println("Control400: Controller");
		return model;
	}
	/*
	@RequestMapping(value = "/teachergohome2", method = RequestMethod.GET)
	public ModelAndView goHome2(ModelAndView model){
		
		Contact teacher = contactDAO.get(userid2);
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");
		return model;	
	}
	*/
	@RequestMapping(value = "/teachergohome3", method = RequestMethod.GET)
	public ModelAndView goHome3(ModelAndView model, @ModelAttribute Contact teacher){
		
		Contact teacher2 = contactDAO.get(teacher.getUserid());
		model.addObject("teacher", teacher2);
		model.setViewName("/teacher/teacherwelcome");
		return model;	
	}
	@RequestMapping(value = "/editteacher", method = RequestMethod.GET)
	public ModelAndView editStudent(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		//Integer userid = Integer.parseInt(request.getParameter("userid"));
		
		Contact teacher = contactDAO.get(id, "edit");
		//System.out.println("editteacher: " + teacher);
		ModelAndView model = new ModelAndView("/teacher/teacher_form");		
		model.addObject("teacher", teacher);		
		return model;
	}
	@RequestMapping(value = "/saveteacher", method = RequestMethod.POST)
	public ModelAndView saveTeacher(ModelAndView model, @ModelAttribute Contact teacher){
		//System.out.println("Kont100: TeacherController" + teacher);
		if (teacher.getId() == null) {
			contactDAO.save(teacher);
		}else {
			
			contactDAO.update(teacher);
		}
        //System.out.println("saveteacher: " + teacher);
		Contact teacher2 = contactDAO.get(teacher.getId(), "string");
		model.addObject("teacher", teacher2);
		//System.out.println("saveteacher: " + teacher2);
		model.setViewName("/teacher/teacherwelcome");
		return model;
		
	}
	
}
