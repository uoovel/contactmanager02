package net.codejava.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.EcriteriaDAO;
import net.codejava.contact.dao.RatingDAO;
//import net.codejava.contact.dao.CriteriaDAO;
//import net.codejava.contact.dao.PropertyDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Ecriteria;
import net.codejava.contact.model.Rating;
import net.codejava.contact.model.RatingView;
//import net.codejava.contact.model.Criteria;
//import net.codejava.contact.model.Property;
//import net.codejava.contact.model.PropertyView;
import net.codejava.contact.model.Student;

@Controller
public class RatingController {
	@Autowired
	private RatingDAO ratingDAO;
	
	@Autowired
	private StudentDAO studentDAO;
	
	@Autowired
	private EcriteriaDAO ecriteriaDAO;	
	
	@Autowired
	private ContactDAO contactDAO;
	
	//PackageTypes
	@RequestMapping(value = "/ratings", method = RequestMethod.GET)
	public ModelAndView listRating(ModelAndView model, HttpServletRequest request) {
        Integer studentid1 = Integer.parseInt(request.getParameter("studentid"));		
        Integer teacherid = Integer.parseInt(request.getParameter("teacherid"));
        Contact teacher = contactDAO.get(teacherid, "string");
        //System.out.println("ratings: " + teacherid);
		//PropertyView propertyview = ratingDAO.get(id);
		
		//properties
		List<Rating> listRating = ratingDAO.list();
		//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
		model.addObject("listRating", listRating); //the left argument is correspondent to field in .jsp
		//////////
		List<RatingView> listRatingView = new ArrayList<>();
		
		for (int i = 0; i < listRating.size(); i++){		
			
			Rating rating = listRating.get(i);
			
			Integer id = rating.getId();//*
			
			Integer studentid = rating.getStudentid();						
			Student student = studentDAO.get(studentid, "string");
			String studname = student.getStudname();//*
			
			Integer ecriteriaid = rating.getEcriteriaid();						
			Ecriteria ecriteria = ecriteriaDAO.get(ecriteriaid);
			String ecritname = ecriteria.getEcritname();//*
			
			//Integer teacherid = studypackage.getTeacherid();						
			//Contact teacher = contactDAO.get(teacherid);
			//String name = teacher.getName();//*
			
			//double score = Math.round(rating.getScore(),1);
			//double score = Math.round(rating.getScore());
			double score = rating.getScore();
			//System.out.println()
			//boolean available = studypackage.getAvailable();
			
			RatingView ratingview = new RatingView(
					id, studname, ecritname, score, studentid, teacherid);
			
			if(studentid1==studentid) {
				listRatingView.add(ratingview);
			}
		};//for
		
		//System.out.println(listStudyPackageView);
		if(listRatingView.isEmpty()) {
			Student student = studentDAO.get(studentid1, "string");
			RatingView ratingview = new RatingView(
					0, student.getStudname(), "not rated yet", 0, studentid1, teacherid);
			listRatingView.add(ratingview);
		}
		
		model.addObject("listRatingView", listRatingView);
		Student student = studentDAO.get(studentid1, "string");
		model.addObject("student", student);
		model.addObject("teacher", teacher);
		
		
		///////////
		model.setViewName("teacher/ratings");
		
		return model;
	}	
	@RequestMapping(value = "/newrating", method = RequestMethod.GET)
	public ModelAndView newRating(ModelAndView model, @ModelAttribute Contact teacher, HttpServletRequest request) {
        Integer studentid1 = Integer.parseInt(request.getParameter("studentid"));		
        Integer teacherid = Integer.parseInt(request.getParameter("teacherid"));
		//List<Integer> listSubject = new ArrayList<Integer>();//my
		//listSubject.add(2);//my
		List<Student> listStudent = studentDAO.list(); //my		
		model.addObject("listStudent", listStudent);//my
		List<Ecriteria> listEcriteria = ecriteriaDAO.list(); //my		
		model.addObject("listEcriteria", listEcriteria);//my		
		List<Contact> listTeacher = contactDAO.list(teacher.getId(), "string"); //my		
		model.addObject("listTeacher", listTeacher);//my			
		
		Student student = studentDAO.get(studentid1, "string");
		model.addObject("student", student);
		//Contact teacher2 = contactDAO.get(teacher.getId(), "string");
		Contact teacher2 = contactDAO.get(teacherid, "string");
		model.addObject("teacher", teacher2);
		
		Rating newRating = new Rating();
		model.addObject("rating", newRating);
		
		
		model.setViewName("teacher/rating_form");	
		//System.out.println("Kont600: StudyPackageController /newstudypackage: ");
		return model;
		
	}
	@RequestMapping(value = "/saverating", method = RequestMethod.POST)
	public ModelAndView saveRating(ModelAndView model, @ModelAttribute Rating rating){
		//System.out.println("Kont700: StudyPackageController" + studypackage);
		if (rating.getId() == null) {
			ratingDAO.save(rating);
		}else {
			
			ratingDAO.update(rating);
			
		}
		Integer teacherid = rating.getTeacherid();
		Contact teacher2 = contactDAO.get(teacherid, "string");
		model.addObject("teacher", teacher2);
		//System.out.println("saveteacher: " + teacher2);
		model.setViewName("/teacher/teacherwelcome");
		//rating.g
		return model;
	}
	
	@RequestMapping(value = "/editrating", method = RequestMethod.GET)
	public ModelAndView editRating(HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));		
		
		Rating rating = ratingDAO.get(id);
		//System.out.println("Kont800: MainController: " + request.getParameter("id"));
		//System.out.println("Kont805: MainController: " + studypackage);
		
		ModelAndView model = new ModelAndView("teacher/rating_edit");	
		
		
		model.addObject("rating", rating);
		
		
		Integer studentid = rating.getStudentid(); //my
		//System.out.println("Kont810: MainController: " + subjectid);
		Student student = studentDAO.get(studentid, "string"); //my		
		model.addObject("student", student);//my		
		List<Student> listStudent = studentDAO.list(); //my		
		model.addObject("listStudent", listStudent);//my
		
		
		Integer ecriteriaid = rating.getEcriteriaid(); //my		
		Ecriteria ecriteria = ecriteriaDAO.get(ecriteriaid); //my		
		model.addObject("ecriteria", ecriteria);//my		
		List<Ecriteria> listEcriteria = ecriteriaDAO.list(); //my		
		model.addObject("listEcriteria", listEcriteria);//my		
		
		//Integer teacherid = studypackage.getTeacherid(); //my		
		//Contact teacher = contactDAO.get(teacherid); //my		
		//model.addObject("teacher", teacher);//my		
		//List<Contact> listTeacher = contactDAO.list(); //my		
		//model.addObject("listTeacher", listTeacher);//my		
	
		
		
		//System.out.println("Kont815: MainController: " + subject);
		
		return model;
	}
	
	@RequestMapping(value = "/deleterating", method = RequestMethod.GET)
	public ModelAndView deleteRating(@RequestParam Integer id){
		ratingDAO.delete(id);
		return new ModelAndView("redirect:/teacher/ratings");
	}
}
