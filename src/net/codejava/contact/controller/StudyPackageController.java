package net.codejava.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.PackageTypeDAO;
//import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.dao.StudyPackageDAO;
import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.PackageType;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.StudyPackage;
import net.codejava.contact.model.StudyPackageView;
import net.codejava.contact.model.Subject;
//import net.codejava.contact.model.PackageType;



@Controller
public class StudyPackageController {
	@Autowired
	private StudyPackageDAO studypackageDAO;
	
	@Autowired
	private SubjectDAO subjectDAO;
	
	@Autowired
	private PackageTypeDAO packagetypeDAO;	
	
	@Autowired
	private ContactDAO contactDAO;
	
	//public Integer userid;
	
	//PackageTypes
	@RequestMapping(value = "/studypackages", method = RequestMethod.GET)
	public ModelAndView listStudyPackage(ModelAndView model, @ModelAttribute Contact teacher) {
		
		//userid = teacher.getUserid();
		Contact teacher1 = contactDAO.get(teacher.getUserid());		
		List<StudyPackage> listStudyPackage = studypackageDAO.list();
		//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
		model.addObject("listStudyPackage", listStudyPackage); //the left argument is correspondent to field in .jsp
		//////////
		List<StudyPackageView> listStudyPackageView = new ArrayList<>();
		
		for (int i = 0; i < listStudyPackage.size(); i++){		
			
			StudyPackage studypackage = listStudyPackage.get(i);
			
			Integer id = studypackage.getId();//*
			
			Integer subjectid = studypackage.getSubjectid();						
			Subject subject = subjectDAO.get(subjectid);
			String subname = subject.getSubname();//*
			
			Integer packagetypeid = studypackage.getPackagetypeid();						
			PackageType packagetype = packagetypeDAO.get(packagetypeid);
			String packname = packagetype.getPackname();//*
			
			Integer teacherid = studypackage.getTeacherid();						
			Contact teacher2 = contactDAO.get(teacherid, "string");
			String name2 = teacher2.getName();//*
			Integer userid2 = teacher2.getUserid();
			
			double price = studypackage.getPrice();
			boolean available = studypackage.getAvailable();
			
			StudyPackageView studypackageview = new StudyPackageView(
					id, subname, packname, name2, price, available);
			
			if(userid2 == teacher.getUserid()) {
				listStudyPackageView.add(studypackageview);
			}
		};//for
		
		//System.out.println(listStudyPackageView);
		model.addObject("teacher", teacher1);
		
		model.addObject("listStudyPackageView", listStudyPackageView);
		
		
		
		
		///////////
		model.setViewName("/teacher/studypackages");
		
		return model;
	}	
	@RequestMapping(value = "/newstudypackage", method = RequestMethod.GET)
	public ModelAndView newStudyPackage(ModelAndView model, @ModelAttribute Contact teacher) {
		//List<Integer> listSubject = new ArrayList<Integer>();//my
		//listSubject.add(2);//my
		List<Subject> listSubject = subjectDAO.list(); //my		
		model.addObject("listSubject", listSubject);//my
		List<PackageType> listPackagetype = packagetypeDAO.list(); //my		
		model.addObject("listPackagetype", listPackagetype);//my		
		//System.out.println("newstudypackage: " + teacher);
		//List<Contact> listTeacher = contactDAO.list(teacher.getId(), "string"); //my		
		
		//model.addObject("listTeacher", listTeacher);//my			
		Contact teacher2 = contactDAO.get(teacher.getId(), "string");
		
		
		model.addObject("teacher", teacher2);
		
		StudyPackage newStudyPackage = new StudyPackage();
		model.addObject("studypackage", newStudyPackage);
		
		
		model.setViewName("/teacher/studypackage_form");	
		//System.out.println("Kont600: StudyPackageController /newstudypackage: ");
		return model;
		
	}
	@RequestMapping(value = "/savestudypackage", method = RequestMethod.POST)
	public ModelAndView saveStudyPackage(ModelAndView model, @ModelAttribute StudyPackage studypackage){
		//System.out.println("Kont700: StudyPackageController" + studypackage);
		if (studypackage.getId() == null) {
			studypackageDAO.save(studypackage);
		}else {
			
			studypackageDAO.update(studypackage);
			
		}
		Integer teacherid = studypackage.getTeacherid();
		Contact teacher = contactDAO.get(teacherid, "string");
		System.out.println("Kont700: StudyPackageController" + teacher);
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");
		return model;
	}
	
	@RequestMapping(value = "/editstudypackage", method = RequestMethod.GET)
	public ModelAndView editStudyPackage(HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));		
		
		StudyPackage studypackage = studypackageDAO.get(id);
		//System.out.println("Kont800: MainController: " + request.getParameter("id"));
		//System.out.println("Kont805: MainController: " + studypackage);
		
		ModelAndView model = new ModelAndView("teacher/studypackage_edit");	
		
		
		model.addObject("studypackage", studypackage);
		
		
		Integer subjectid = studypackage.getSubjectid(); //my
		//System.out.println("Kont810: MainController: " + subjectid);
		Subject subject = subjectDAO.get(subjectid); //my		
		model.addObject("subject", subject);//my		
		List<Subject> listSubject = subjectDAO.list(); //my		
		model.addObject("listSubject", listSubject);//my
		
		
		Integer packagetypeid = studypackage.getPackagetypeid(); //my		
		PackageType packagetype = packagetypeDAO.get(packagetypeid); //my		
		model.addObject("packagetype", packagetype);//my		
		List<PackageType> listPackagetype = packagetypeDAO.list(); //my		
		model.addObject("listPackagetype", listPackagetype);//my		
		
		Integer teacherid = studypackage.getTeacherid(); //my		
		Contact teacher = contactDAO.get(teacherid, "string"); //my		
		model.addObject("teacher", teacher);//my		
		List<Contact> listTeacher = contactDAO.list(teacherid, "string"); //my		
		model.addObject("listTeacher", listTeacher);//my		
	
		
		
		//System.out.println("Kont815: MainController: " + subject);
		
		return model;
	}
	
	@RequestMapping(value = "/deletestudypackage", method = RequestMethod.GET)
	public ModelAndView deleteStudyPackage(ModelAndView model, @RequestParam Integer id){
		StudyPackage studypackage = studypackageDAO.get(id);
		Integer teacherid = studypackage.getTeacherid();
		studypackageDAO.delete(id);
		
		Contact teacher = contactDAO.get(teacherid, "string");		
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");
		return model;
	}
	/*
	@RequestMapping(value = "/teachergohome", method = RequestMethod.GET)
	public ModelAndView saveStudentPack(ModelAndView model){
		
		Contact teacher = contactDAO.get(userid);
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");
		return model;	
	}*/
}
