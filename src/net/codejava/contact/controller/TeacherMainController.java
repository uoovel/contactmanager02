package net.codejava.contact.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.UserDAO;
import net.codejava.contact.model.User;


@Controller
public class TeacherMainController {

	@Autowired
	private UserDAO userDAO;
	
	@RequestMapping(value ="/teacherjoyn")
	public ModelAndView teacherfrontPage(ModelAndView model) {
		model.setViewName("/teacher/teacherindex");
		return model;
	}
	
    @RequestMapping(value="/teacherregister")
    public ModelAndView register(ModelAndView model) {
    	User newUser = new User();
    	model.addObject("user", newUser);
    	model.setViewName("/teacher/teacherregister_form");
        return model;
    }
    
    //registering method
	@RequestMapping(value = "/registerteacher", method = RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute User user){
		String password = user.getPassword();
		String encryptedPass = CryptPass.encrypt("JavasEncryptDemo", "RandomJavaVector", password);
		user.setPassword(encryptedPass);
		
		List<User> listUser = userDAO.list();		
		boolean exists1 = false;
		String userEmail = user.getEmail();
		//boolean autenditud2 = false;
		User userr = new User();
		for(int i = 0; i<listUser.size(); i++) {
			userr = listUser.get(i);
			String email = userr.getEmail();
			if(userEmail.equals(email)) {
				exists1 = true;
				break;
			}			
		}
		if(exists1 != true) {
			if (user.getId() == null) {
				userDAO.save(user);
			}else {
				//System.out.println("Kont200: MainController" + contact);
				userDAO.update(user);
			}		
			//return new ModelAndView("redirect:/loginn");
			return new ModelAndView("redirect:/teacherloginn");
		} else {
			return new ModelAndView("redirect:/exists");
		}
	}
	
    @RequestMapping(value="/teacherloginn")
    public ModelAndView loginn(ModelAndView model) {
    	User newUser = new User();
    	model.addObject("user", newUser);
    	model.setViewName("/teacher/teacherlogin_form");
        return model;
    }
    
    @RequestMapping(value="/teacherwelcome", method = RequestMethod.GET)
    public ModelAndView welcome() {
    	//System.out.println("MainController,welcome");
    	//ModelAndView model = new ModelAndView();
    	//Student student = new Student();    	
    	//student = studentDAO.get(userid);
    	//model.addObject("student", student);
		//model.setViewName("welcome");
    	//return model;
        return new ModelAndView("/teacher/teacherwelcome");
    }	

}
