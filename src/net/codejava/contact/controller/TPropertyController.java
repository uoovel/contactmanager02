package net.codejava.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.CriteriaDAO;
import net.codejava.contact.dao.PropertyDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.StudentPackDAO;
import net.codejava.contact.dao.StudyPackageDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Criteria;
import net.codejava.contact.model.Property;
import net.codejava.contact.model.PropertyView;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.StudentPack;
import net.codejava.contact.model.StudyPackage;


@Controller
public class TPropertyController {
	@Autowired
	private PropertyDAO propertyDAO;
	
	@Autowired
	private StudentDAO studentDAO;
	
	@Autowired
	private CriteriaDAO criteriaDAO;	
	 
	@Autowired
	private ContactDAO contactDAO;
	
	@Autowired
	StudentPackDAO studentpackDAO;
	
	@Autowired
	StudyPackageDAO studypackageDAO;	
	
	//Integer userid;
	//@Autowired
	//private ContactDAO contactDAO;
	
	//PackageTypes
	
	@RequestMapping(value = "/tviewprofile", method = RequestMethod.GET)
	public ModelAndView listProperty(ModelAndView model, HttpServletRequest request) {
		//properties
		Integer studentpackid = Integer.parseInt(request.getParameter("id"));
		StudentPack studentpack = studentpackDAO.get(studentpackid);
		Integer studentid = studentpack.getStudentid();
		Integer studypackageid = studentpack.getStudypackageid();
		StudyPackage studypackage = studypackageDAO.get(studypackageid);
		Integer teacherid = studypackage.getTeacherid();
		Contact teacher = contactDAO.get(teacherid, "string");
		//System.out.println("Kont500: TPropertyController: " + teacher);
		List<Property> listProperty = propertyDAO.list();
		
		model.addObject("listProperty", listProperty); //the left argument is correspondent to field in .jsp
		//////////
		List<PropertyView> listPropertyView = new ArrayList<>();
		
		for (int i = 0; i < listProperty.size(); i++){		
			
			Property property = listProperty.get(i);
			
			Integer id = property.getId();//*
			
			Integer studentid2 = property.getStudentid();						
			Student student2 = studentDAO.get(studentid, "string");
			String studname = student2.getStudname();//*
			
			Integer criteriaid = property.getCriteriaid();						
			Criteria criteria = criteriaDAO.get(criteriaid);
			String critname = criteria.getCritname();//*
			
			//Integer teacherid = studypackage.getTeacherid();						
			//Contact teacher = contactDAO.get(teacherid);
			//String name = teacher.getName();//*
			
			String description = property.getDescription();
			//boolean available = studypackage.getAvailable();
			
			PropertyView propertyview = new PropertyView(
					id, studname, critname, description, studentid2, teacherid);
			if(studentid2 == studentid) {
				listPropertyView.add(propertyview);
			}
			
		};//for
		
		//System.out.println("tviewprofile: " + listPropertyView);
		
		
		model.addObject("listPropertyView", listPropertyView);
		Student student =studentDAO.get(studentid, "string");
		model.addObject("student", student);
		
		model.addObject("teacher", teacher);
		
		model.setViewName("teacher/tpropertiesfalse");
		///////////
		if(teacher.getVerified()==true) {
			model.setViewName("teacher/tproperties");
		}
		
		return model;
	}
	
	
	

}
