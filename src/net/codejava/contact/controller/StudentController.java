package net.codejava.contact.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.CriteriaDAO;
import net.codejava.contact.dao.PropertyDAO;
//import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.UserDAO;
import net.codejava.contact.model.Criteria;
import net.codejava.contact.model.Property;
import net.codejava.contact.model.PropertyView;
//import net.codejava.contact.model.Contact;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.User;

@Controller
public class StudentController {
	@Autowired
	private StudentDAO studentDAO;
	@Autowired
	private UserDAO userDAO;
	@Autowired
	private PropertyDAO propertyDAO;
	@Autowired
	private CriteriaDAO criteriaDAO;    
	//Integer userid2;
	
	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public ModelAndView listStudent(ModelAndView model, @ModelAttribute Student student) {
		//System.out.println("Control102: Controller" + student);
		Integer userid = student.getUserid();
		//userid2 = userid;
		
		//if(id==null) {
		//	id = student.getUserid();
		//}
		//id = student.getUserid();
		//id = user.getId();
		List<Student> listStudent = studentDAO.list(userid);
		//List<Student> listStudent = studentDAO.list(1); //siia user_id parsida
		//System.out.println("Control200: Controller");
		model.addObject("listStudent", listStudent);
		//System.out.println("Control300: Controller");
		model.setViewName("students");	
		//System.out.println("Control400: Controller");
		
		//properties
		List<Property> listProperty = propertyDAO.list();
		//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
		model.addObject("listProperty", listProperty); //the left argument is correspondent to field in .jsp
		//////////
		List<PropertyView> listPropertyView = new ArrayList<>();
		
		for (int i = 0; i < listProperty.size(); i++){		
			
			Property property = listProperty.get(i);
			
			Integer id = property.getId();//*
			
			Integer studentid = property.getStudentid();						
			Student student2 = studentDAO.get(studentid, "string");
			String studname2 = student2.getStudname();//*
			
			Integer criteriaid = property.getCriteriaid();						
			Criteria criteria = criteriaDAO.get(criteriaid);
			String critname = criteria.getCritname();//*
			
			//Integer teacherid = studypackage.getTeacherid();						
			//Contact teacher = contactDAO.get(teacherid);
			//String name = teacher.getName();//*
			
			String description = property.getDescription();
			//boolean available = studypackage.getAvailable();
			
			PropertyView propertyview = new PropertyView(
					id, studname2, critname, description, studentid, null);
			
			if(student2.getUserid()==userid) {
				listPropertyView.add(propertyview);
			}
			
		};//for
		
		
		student = studentDAO.get(userid);
		
		model.addObject("listPropertyView", listPropertyView);
		
		model.addObject("student", student);
		
		//System.out.println("StudentController: " + student);
		
		///////////
		
		return model;
	}
	/*
	@RequestMapping(value = "/students2")
	public ModelAndView listStudent2(ModelAndView model) {
		//System.out.println("Control100: Controller");
		//id = user.getId();
		//Integer userid = student.getUserid();
		List<Student> listStudent = studentDAO.list(userid2);
		//List<Student> listStudent = studentDAO.list(1); //siia user_id parsida
		//System.out.println("Control200: Controller");
		model.addObject("listStudent", listStudent);
		//System.out.println("Control300: Controller");
		model.setViewName("students");	
		//System.out.println("Control400: Controller");
		return model;
	}*/
	
	@RequestMapping(value = "/administration/newstudent", method = RequestMethod.GET)
	public ModelAndView newStudent(ModelAndView model) {
		Student newStudent = new Student();
		model.addObject("student", newStudent);
		model.setViewName("/administration/student_form");		
		return model;
		
	}
	@RequestMapping(value = "/savestudent", method = RequestMethod.POST)
	public ModelAndView saveStudent(ModelAndView model, @ModelAttribute Student student){
		if (student.getId() == null) {
			studentDAO.save(student);
		}else {
			//System.out.println("Kont200: MainController" + contact);
			studentDAO.update(student);
		}
		//ModelAndView model = new ModelAndView();
		
		//Integer userid = student.getUserid();
		//User userr = userDAO.get(userid);
		//Student student2 = new Student();
		//student2 = studentDAO.get(student.getId(), "edit");
		//System.out.println("Control700: Controller, saveStudent: " + student2);
		//model.addObject("student", student2);
		
		//List<Student> listStudent = studentDAO.list(student.getUserid());
		//List<Student> listStudent = studentDAO.list(1); //siia user_id parsida
		//System.out.println("Control800: Controller");
		//model.addObject("listStudent", listStudent);
		
		
		//model.setViewName("students2");		
		//return new ModelAndView("redirect:students");
		
		//return new ModelAndView("redirect:/students2");
		
		Student student2 = studentDAO.get(student.getId(), "string");
		model.addObject("student", student2);
		model.setViewName("/welcome");
		return model;
		
	}
	
	@RequestMapping(value = "/editstudent", method = RequestMethod.GET)
	public ModelAndView editStudent(HttpServletRequest request){
		Integer id = Integer.parseInt(request.getParameter("id"));
		//Integer userid = Integer.parseInt(request.getParameter("userid"));
		
		Student student = studentDAO.get(id, "edit");
		ModelAndView model = new ModelAndView("student_form");		
		model.addObject("student", student);		
		return model;
	}
	@RequestMapping(value = "/gohome2", method = RequestMethod.GET)
	public ModelAndView goHome2(ModelAndView model, @ModelAttribute Student student){
		
		Integer userid = student.getUserid();
		student = studentDAO.get(userid);
		model.addObject("student", student);
		model.setViewName("/welcome");
		return model;	
	}

}
