package net.codejava.contact.controller;

import java.sql.Date;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.codejava.contact.dao.ContactDAO;
import net.codejava.contact.dao.PackageTypeDAO;
import net.codejava.contact.dao.RatingDAO;
import net.codejava.contact.dao.StudentDAO;
import net.codejava.contact.dao.StudentPackDAO;
import net.codejava.contact.dao.StudyPackageDAO;
import net.codejava.contact.dao.SubjectDAO;
import net.codejava.contact.model.Contact;
import net.codejava.contact.model.PackageType;
import net.codejava.contact.model.Rating;
import net.codejava.contact.model.Student;
import net.codejava.contact.model.StudentPack;
import net.codejava.contact.model.StudentPackView;
import net.codejava.contact.model.StudyPackage;
import net.codejava.contact.model.StudyPackageView;
import net.codejava.contact.model.Subject;



@Controller
public class TStudentPackController {
	
	
	@Autowired
	ContactDAO contactDAO;
	@Autowired
	StudentPackDAO studentpackDAO;
	@Autowired
	StudyPackageDAO studypackageDAO;
	@Autowired
	SubjectDAO subjectDAO;
	@Autowired
	PackageTypeDAO packagetypeDAO;
	@Autowired
	StudentDAO studentDAO;
	@Autowired
	RatingDAO ratingDAO;
	
	//Integer userid;
	//Integer userid2;
	
	@RequestMapping(value = "/teacherstudentpacks", method = RequestMethod.GET)
	public ModelAndView listStudentPack(ModelAndView model, @ModelAttribute Contact teacher) {
		//System.out.println("teacherstudentpacks : " + teacher);
		//userid = teacher.getUserid();
		//Variables.userid3 = teacher.getUserid();
		
		Contact teacher1 = contactDAO.get(teacher.getUserid());
		List<StudentPack> listStudentPack = studentpackDAO.list();
		
		model.addObject("listStudentPack", listStudentPack); //the left argument is correspondent to field in .jsp
		//////////
		List<StudentPackView> listStudentPackView = new ArrayList<StudentPackView>();
		
		for (int i = 0; i < listStudentPack.size(); i++){		
			
			StudentPack studentpack = listStudentPack.get(i);
			
			Integer id = studentpack.getId();//*
			
			//studypackageview plokk
			Integer studypackageid = studentpack.getStudypackageid();//*						
			StudyPackage studypackage = studypackageDAO.get(studypackageid);
			
			Integer subjectid = studypackage.getSubjectid();
			Subject subject = subjectDAO.get(subjectid);
			String subname = subject.getSubname();//*	
			
			Integer packagetypeid = studypackage.getPackagetypeid();
			PackageType packagetype = packagetypeDAO.get(packagetypeid);
			String packname = packagetype.getPackname();//*			

			Integer teacherid = studypackage.getTeacherid();
			Contact teacher2 = contactDAO.get(teacherid, "string");
			String name2 = teacher2.getName();//*
			Integer userid2 = teacher2.getUserid();

			Double price = studypackage.getPrice();//*	
			
			Boolean available = studypackage.getAvailable();
			
			StudyPackageView studypackageview = new StudyPackageView(studypackageid, subname, packname, name2, price, available);
            //
			
			Integer studentid = studentpack.getStudentid();						
			Student student2 = studentDAO.get(studentid, "edit");
			String studname2 = student2.getStudname();//*
			//Integer userid2 = student2.getUserid();
			
			//rating
			
			Rating rating = ratingDAO.getAverScore(studentid);
			double score = rating.getScore();
			//double score = Math.round(rating.getScore());
			DecimalFormat df = new DecimalFormat("#.#");
			String score1 = df.format(score);
			//double score2 = Double.valueOf(score1);
			//System.out.println("teacherstudentpacks: " + score + "student: " + studname2);
			
			//rating
			
			Date begin = studentpack.getBegin();			
			
			StudentPackView studentpackview = new StudentPackView(id, studypackageview, studname2, begin, studentid, score1);
			//Järg
			//System.out.println("Kont500: StudyPackageController: " + listStudyPackage);
			if(userid2 == teacher.getUserid()) {
				listStudentPackView.add(studentpackview);
			}
			
		};
		
		//System.out.println(listStudentPackView);
		//https://www.journaldev.com/16094/java-collections-sort
		//Collections.sort(listStudentPackView, (a, b) -> {
		//    return a.getScore().compareTo(b.getScore());
		//});
		Collections.sort(listStudentPackView, Collections.reverseOrder((a, b) -> {
		    return a.getScore().compareTo(b.getScore());
		}));
		
		//Collections.sort(listStudentPackView, Collections.reverseOrder());
		
		
		//Collections.sort((List<T>) listStudentPackView);;
		
		model.addObject("teacher", teacher1);
		
		model.addObject("listStudentPackView", listStudentPackView);
		
		
		
		
		///////////
		model.setViewName("/teacher/teacherstudentpacks");
		
		return model;
	}
	@RequestMapping(value = "/teditstudentpack", method = RequestMethod.GET)
	public ModelAndView editStudentPack(HttpServletRequest request){
		
		Integer id = Integer.parseInt(request.getParameter("id"));		
		System.out.println("editstudentpack: " + id);
		StudentPack studentpack = studentpackDAO.get(id);
		//System.out.println("Kont800: MainController: " + request.getParameter("id"));
		//System.out.println("Kont805: MainController: " + studypackage);
		
		ModelAndView model = new ModelAndView("/teacher/tstudentpack_edit");	
		
		
		model.addObject("studentpack", studentpack);
		
		
		Integer studypackageid = studentpack.getStudypackageid(); //my
		//System.out.println("Kont810: MainController: " + subjectid);
		StudyPackage studypackage = studypackageDAO.get(studypackageid); //my		
		model.addObject("studypackage", studypackage);//my		
		List<StudyPackage> listStudypackage = studypackageDAO.list(); //my		
		model.addObject("listStudypackage", listStudypackage);//my
		
		//studypackageview
		Integer subjectid = studypackage.getSubjectid();						
		Subject subject = subjectDAO.get(subjectid);
		String subname = subject.getSubname();//*
		
		Integer packagetypeid = studypackage.getPackagetypeid();						
		PackageType packagetype = packagetypeDAO.get(packagetypeid);
		String packname = packagetype.getPackname();//*
		
		Integer teacherid = studypackage.getTeacherid();						
		Contact teacher = contactDAO.get(teacherid, "string");
		String name = teacher.getName();//*
		
		double price = studypackage.getPrice();
		boolean available = studypackage.getAvailable();
        StudyPackageView studypackageview = new StudyPackageView(studypackageid, subname, packname, name, price, available);
        model.addObject("studypackageview", studypackageview);
		
		//////////liststudypackageview
		List<StudyPackageView> listStudyPackageView = new ArrayList<>();
		
		for (int i = 0; i < listStudypackage.size(); i++){		
			
			studypackage = listStudypackage.get(i);
			
			id = studypackage.getId();//*
			
			subjectid = studypackage.getSubjectid();						
			subject = subjectDAO.get(subjectid);
			subname = subject.getSubname();//*
			
			packagetypeid = studypackage.getPackagetypeid();						
			packagetype = packagetypeDAO.get(packagetypeid);
			packname = packagetype.getPackname();//*
			
			Integer teacherid2 = studypackage.getTeacherid();						
			Contact teacher2 = contactDAO.get(teacherid2, "string");
			String name2 = teacher2.getName();//*
			
			price = studypackage.getPrice();
			available = studypackage.getAvailable();
			if(teacherid==teacherid2) {
				studypackageview = new StudyPackageView(id, subname, packname, name2, price, available);
				listStudyPackageView.add(studypackageview);
			}
		};
		
		
		//System.out.println("StudentPackController, editStudentPack 500:" + listStudyPackageView);
		
		
		model.addObject("listStudyPackageView", listStudyPackageView);
		
		
		
		
		///////////		
		
		
		
		
		
		
		Integer studentid = studentpack.getStudentid(); //my		
		Student student = studentDAO.get(studentid, "string"); //my		
		model.addObject("student", student);//my		
		List<Student> listStudent = studentDAO.listByid(studentid); //my		
		model.addObject("listStudent", listStudent);//my		
	
		
		
		//System.out.println("Kont815: MainController: " + subject);
		
		return model;
	}
	@RequestMapping(value = "/tsavestudentpack", method = RequestMethod.POST)
	public ModelAndView tsaveStudentPack(ModelAndView model, @ModelAttribute StudentPack studentpack){
		
		if (studentpack.getId() == null) {
			studentpackDAO.save(studentpack);
		}else {
			
			studentpackDAO.update(studentpack);
			
		}		
		//System.out.println("Kont700: StudyPackageController" + userid);
		//User user = userDAO.get(userid);		
		//model.addObject("user", user);
		Integer studypackageid = studentpack.getStudypackageid();
		StudyPackage studypackage = studypackageDAO.get(studypackageid);
		Integer teacherid = studypackage.getTeacherid();
		Contact teacher = contactDAO.get(teacherid, "string");
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");
		return model;
		
		//return new ModelAndView("redirect:/studentpacks");
	}
	@RequestMapping(value = "/tdeletestudentpack", method = RequestMethod.GET)
	public ModelAndView deleteStudentPack(ModelAndView model, @RequestParam Integer id){
		StudentPack studentpack = studentpackDAO.get(id);
		Integer studypackageid = studentpack.getStudypackageid();
		StudyPackage studypackage = studypackageDAO.get(studypackageid);
		Integer teacherid = studypackage.getTeacherid();
		
		studentpackDAO.delete(id);
		
		Contact teacher = contactDAO.get(teacherid, "string");		
		model.addObject("teacher", teacher);
		model.setViewName("/teacher/teacherwelcome");		
		
		return model;
		//return new ModelAndView("redirect:/studentpacks");
	}	

}
