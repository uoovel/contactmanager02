package net.codejava.contact.model;

import java.sql.Date;

public class StudentPackView {
	private Integer id;	
	private StudyPackageView studypackageview;
	private String studname;
	private Date begin;
	private Integer studentid;
	private String score;
	
	public StudentPackView() {
		
	}

	public StudentPackView(Integer id, StudyPackageView studypackageview, String studname,
			Date begin, Integer studentid, String score) {		
		
		this(studypackageview, studname, begin, studentid, score);
		this.id = id;
	}
	public StudentPackView(StudyPackageView studypackageview, String studname,
			Date begin, Integer studentid, String score) {
		this.studypackageview = studypackageview;
		this.studname = studname;
		this.begin = begin;
		this.studentid = studentid;
		this.score = score;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public StudyPackageView getStudypackageview() {		
		return studypackageview;
	}
	public void setStudypackageview(StudyPackageView studypackageview) {
		this.studypackageview = studypackageview;
	}
	public String getStudname() {		
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	
	public Date getBegin() {		
		return begin;
	}
	public void setBegin(Date begin) {
		this.begin = begin;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public String getScore() {
		return score;
	}
	public void setScore(String score) {
		this.score = score;
	}	

	@Override
	public String toString() {
		return "StudentPackView [id=" + id + ", "
				+ "studypackageview=" + studypackageview + ", "
				+ "studname=" + studname + ", "
				+ "begin=" + begin
				+ "score=" + score
				+ "]";
	}
}
