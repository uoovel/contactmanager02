package net.codejava.contact.model;

public class StudyPackage {
	private Integer id;
	private Integer subjectid;	
	private Integer packagetypeid;
	private Integer teacherid;
	private double price;
	private boolean available;

	
	public StudyPackage() {
		
	}

	public StudyPackage(Integer id, Integer subjectid, Integer packagetypeid,
			Integer teacher_id, double price, boolean available) {		
		
		this(subjectid, packagetypeid, teacher_id, price, available);
		this.id = id;
	}
	public StudyPackage(Integer subjectid, Integer packagetypeid,
			Integer teacher_id, double price, boolean available) {
		this.subjectid = subjectid;
		this.packagetypeid = packagetypeid;
		this.teacherid = teacher_id;
		this.price = price;
		this.available = available;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSubjectid() {		
		return subjectid;
	}
	public void setSubjectid(Integer subjectid) {
		this.subjectid = subjectid;
	}
	public Integer getPackagetypeid() {		
		return packagetypeid;
	}
	public void setPackagetypeid(Integer packagetypeid) {
		this.packagetypeid = packagetypeid;
	}
	public Integer getTeacherid() {		
		return teacherid;
	}
	public void setTeacherid(Integer teacher_id) {
		this.teacherid = teacher_id;
	}
	public double getPrice() {		
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean getAvailable() {		
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return "StudyPackage [id=" + id + ", "
				+ "subjectid=" + subjectid + ", "
				+ "packagetypeid=" + packagetypeid + ", "
				+ "teacherid=" + teacherid + ", "
				+ "price=" + price + ", "
				+ "available=" + available
				+ "]";
	}
}
