package net.codejava.contact.model;

import java.sql.Date;

public class Property {
	private Integer id;
	private Integer studentid;
	private Integer criteriaid;		
	private String description;

	
	public Property() {
		
	}

	public Property(Integer id, Integer studentid, Integer criteriaid,
			 String description) {		
		
		this(studentid, criteriaid, description);
		this.id = id;
	}
	public Property(Integer studentid, Integer criteriaid,
			String description) {
		this.studentid = studentid;
		this.criteriaid = criteriaid;
		this.description = description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStudentid() {		
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getCriteriaid() {		
		return criteriaid;
	}
	public void setCriteriaid(Integer criteriaid) {
		this.criteriaid = criteriaid;
	}
	public String getDescription() {		
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Property [id=" + id + ", "
				+ "studentid=" + studentid + ", "
				+ "criteriaid=" + criteriaid + ", "
				+ "description=" + description
				+ "]";
	}
}
