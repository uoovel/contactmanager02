package net.codejava.contact.model;

public class Rating {
	private Integer id;
	private Integer studentid;
	private Integer ecriteriaid;		
	private double score;
	private Integer teacherid;

	
	public Rating() {
		
	}

	public Rating(Integer id, Integer studentid, Integer ecriteriaid ,
			double score, Integer teacherid) {		
		
		this(studentid, ecriteriaid, score, teacherid);
		this.id = id;
	}
	public Rating(Integer studentid, Integer ecriteriaid,
			double score, Integer teacherid) {
		this.studentid = studentid;
		this.ecriteriaid = ecriteriaid;		
		this.score = score;
		this.teacherid = teacherid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getStudentid() {		
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getEcriteriaid() {		
		return ecriteriaid;
	}
	public void setEcriteriaid(Integer ecriteriaid) {
		this.ecriteriaid = ecriteriaid;
	}
	public double getScore() {		
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public Integer getTeacherid() {		
		return teacherid;
	}
	public void setTeacherid(Integer teacherid) {
		this.teacherid = teacherid;
	}
	@Override
	public String toString() {
		return "Rating [id=" + id + ", "
				+ "studentid=" + studentid + ", "
				+ "ecriteriaid=" + ecriteriaid + ", "
				+ "score=" + score + ", "
				+ "teacherid=" + teacherid
				+ "]";
	}
}
