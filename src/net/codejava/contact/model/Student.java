package net.codejava.contact.model;

public class Student {
	private Integer id;
	private String studname;
	private String email;
	private Integer userid;

	
	public Student() {
		
	}

	public Student(Integer id, String studname, String email, Integer userid) {	
		
		this(studname, email, userid);		
		this.id = id;
	}
	public Student(String studname, String email, Integer userid) {
		//System.out.println("Control115: model Student");
		this.studname = studname;
		this.email = email;
		this.userid = userid;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStudname() {
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getUserid() {
		return userid;
	}
	public void setUserid(Integer userid) {
		this.userid = userid;
	}
	
	@Override
	public String toString() {
		return "Student [id=" + id + ", studname=" + studname + ", email=" + email
				+ ", userid=" + userid + "]";
	}
}
