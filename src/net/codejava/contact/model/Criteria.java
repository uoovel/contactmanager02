package net.codejava.contact.model;



public class Criteria {
	private Integer id;
	private String critname;

	
	public Criteria() {
		
	}

	public Criteria(Integer id, String critname) {		
		
		this(critname);
		this.id = id;
	}
	public Criteria(String critname) {
		
		
		this.critname = critname;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCritname() {
		//System.out.println("Kontr520:Subject/getSubname");
		return critname;
	}
	public void setCritname(String critname) {
		this.critname = critname;
	}

	@Override
	public String toString() {
		return "Criteria [id=" + id + ", critname=" + critname
				+ "]";
	}
}