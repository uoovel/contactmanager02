package net.codejava.contact.model;

public class Ecriteria {
	private Integer id;
	private String ecritname;

	
	public Ecriteria() {
		
	}

	public Ecriteria(Integer id, String ecritname) {		
		
		this(ecritname);
		this.id = id;
	}
	public Ecriteria(String ecritname) {
		
		
		this.ecritname = ecritname;

	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEcritname() {
		//System.out.println("Kontr520:Subject/getSubname");
		return ecritname;
	}
	public void setEcritname(String ecritname) {
		this.ecritname = ecritname;
	}

	@Override
	public String toString() {
		return "Ecriteria [id=" + id + ", ecritname=" + ecritname
				+ "]";
	}
}
