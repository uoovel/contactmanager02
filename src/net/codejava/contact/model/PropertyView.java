package net.codejava.contact.model;

public class PropertyView {
	private Integer id;
	private String studname;
	private String critname;
	private String description;
	private Integer studentid;
	private Integer teacherid;
	//private double price;
	//private boolean available;
	
	private String propertyname;
	
	public PropertyView() {
		
	}

	public PropertyView(Integer id, String studname, String critname,
			String description, Integer studentid, Integer teacherid) {		
		
		this(studname, critname, description, studentid, teacherid);
		this.id = id;
	}
	public PropertyView(String studname, String critname,
			String description, Integer studentid, Integer teacherid) {
		this.studname = studname;
		this.critname = critname;
		this.description = description;
		this.studentid = studentid;
		this.teacherid = teacherid;
		this.propertyname = studname + "; " + critname + "; " + description;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getStudname() {		
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	public String getCritname() {		
		return critname;
	}
	public void setCritname(String critname) {
		this.critname = critname;
	}
	public String getDescription() {		
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getStudentid() {
		return studentid;
	}
	public void setStudentid(Integer studentid) {
		this.studentid = studentid;
	}
	public Integer getTeacherid() {
		return teacherid;
	}
	public void setTeacherid(Integer teacherid) {
		this.teacherid = teacherid;
	}	
	public String getPropertyname() {		
		return propertyname;
	}
	public void setPropertyname(String propertyname) {
		this.propertyname = propertyname;
	}
	
	
	@Override
	public String toString() {
		return "PropertyView [id=" + id + ", "
				+ "studname=" + studname + ", "
				+ "critname=" + critname + ", "
				+ "description=" + description + ", "
				+ "studentid=" + studentid + ", "
				+ "teacherid=" + teacherid
				+ "]";
	}
}
