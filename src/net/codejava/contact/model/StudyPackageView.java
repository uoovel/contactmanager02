package net.codejava.contact.model;

public class StudyPackageView {
	private Integer id;
	private String subname;
	private String packname;
	private String name;
	private double price;
	private boolean available;
	
	private String studypackagename;
	
	public StudyPackageView() {
		
	}

	public StudyPackageView(Integer id, String subname, String packname,
			String name, double price, boolean available) {		
		
		this(subname, packname, name, price, available);
		this.id = id;
	}
	public StudyPackageView(String subname, String packname,
			String name, double price, boolean available) {
		this.subname = subname;
		this.packname = packname;
		this.name = name;
		this.price = price;
		this.available = available;
		this.studypackagename = subname + "; " + packname + "; " + name + "; " + price;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSubname() {		
		return subname;
	}
	public void setSubname(String subname) {
		this.subname = subname;
	}
	public String getPackname() {		
		return packname;
	}
	public void setPackname(String packname) {
		this.packname = packname;
	}
	public String getName() {		
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {		
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public boolean getAvailable() {		
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	
	public String getStudypackagename() {		
		return studypackagename;
	}
	public void setStudypackagename(String studypackagename) {
		this.studypackagename = studypackagename;
	}
	
	
	@Override
	public String toString() {
		return  "Subject: " + subname + ", <br>"
				+ "Study type: " + packname + ", <br>"
				+ "Teacher: " + name + ", <br>"				
				+ "Price (month): " + price + ", <br>"
				+ "Available: " + available;
				
	}

}
